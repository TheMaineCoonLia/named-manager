/**
 * 
 * Construct a new authenticator
 * 
 */
function Authenticator( type, passphrase, ip )
{
	
	//Store the type
	this.setType( type );
	
	//Store the passphrase
	this.setPassphrase( passphrase );
	
	//Store the ip
	this.setIP( ip );
	
}

/**
 * 
 * This function is used internally to execute the success callback and the completion callback
 * 
 * @param type what type the authenticated user is
 * @since 0.3.0
 * 
 */
Authenticator.prototype.executeSuccess = function( type )
{
	
	//Check that the callback isn't undefined
	if( this.getSuccessCallback( ) !== undefined )
	{
	
		try
		{
			
			//Execute the callback in a try/catch to make sure nothing messes up
			this.getSuccessCallback( )( type );
			
		}
		catch( exception )
		{
			
		}
		
	}
	
	//Execute the completion
	this.executeComplete( undefined, type );
	
};

/**
 * 
 * This function is used internally to execute the error callback and the completion callback
 * 
 * @param error the error that prevented the authentication
 * @since 0.3.0
 * 
 */
Authenticator.prototype.executeError = function( error )
{

	//Check that the callback isn't undefined
	if( this.getErrorCallback( ) !== undefined )
	{
	
		try
		{

			//Execute the callback in a try/catch to make sure nothing messes up
			this.getErrorCallback( )( error );
			
		}
		catch( exception )
		{
			
		}
		
	}

	//Execute the completion
	this.executeComplete( error, undefined );
	
};

/**
 * 
 * This function is used internally to execute the completion callback
 * 
 * @param error the possible error that occoured
 * @param type the type that the user is if the authentication was successful
 * @since 0.3.0
 * 
 */
Authenticator.prototype.executeComplete = function( error, type )
{

	//Check that the callback isn't undefined
	if( this.getCompletionCallback( ) !== undefined )
	{
	
		try
		{

			//Execute the callback in a try/catch to make sure nothing messes up
			this.getCompletionCallback( )( error, type );
			
		}
		catch( exception )
		{
			
		}
		
	}
	
};

/**
 * 
 * This function is used to execute the authentication project
 * @since 0.3.0
 * 
 */
Authenticator.prototype.execute = function( )
{
	
	//Get the users available
	var users = NamedManager.getConfig( ).get( 'users' );
	
	//Do the following in a try/catch in case anything messes up
	try
	{
		
		//Loop through all the users
		for( var i = 0; i < users.length; i++ )
		{
			
			//Again, try/catch block to prevent fire
			try
			{
				
				//Get the user
				var user = users[ i ];
			
				//Verify the information
				if( user.kind === this.getType( ) && user.passphrase === this.getPassphrase( ) )
				{
					
					//Check if there's an IP Restriction
					if( user.ip !== undefined )
					{
						
						//Check if the IP matches
						if( user.ip === this.getIP( ) )
						{
							
							//Successful login. Return the type of the user
							this.executeSuccess( this.getType( ) );
						
						}
						
						//Put this in a try/catch to prevent mess ups
						try
						{
							
							//Loop through the available IP's
							for( var j = 0; j < user.ip.length; j++ )
							{
							
								//Fetch the IP
								var ip = user.ip[ j ];
								
								//See if the IP Match
								if( ip === this.getIP( ) )
								{
									
									//Successful login. Return the type of the user
									this.executeSuccess( this.getType( ) );
									
									return;
									
								}
								
							}
							
						}
						catch( iperror )
						{
							
							console.log(
									
									'[Master] [Authenticator] There\'s an IP Related error at user {i: %i, p: %s, e: %o}',
									i,
									user.passphrase,
									iperror
									
							);
							
						}
						
						//Invalid IP Address!
						this.executeError( new Error( 'Invalid IP' ) );
						
						return;
						
					}
					
					//Successful login. Return the type of the user
					this.executeSuccess( this.getType( ) );
					
					//Stop the loop from continuing
					return;
					
				}
				
			}
			catch( error )
			{
				
				//We don't want to halt the operation because of an possible error with a user.
				//As it's possible that the user requested is in the other object
				console.log(
						
						'[Master] [Authenticator] There\'s an unkown error at user {i: %i, p: %s, e: %o}',
						i,
						user.passphrase,
						error
				
				);
						
			}
			
		}
		
	}
	catch( exception )
	{

		//Execute the error
		this.executeError( exception );
		
	}
	
	//No user was found. Notify
	this.executeError( new Error( 'Invalid User' ) );
	
};

/**
 * 
 * @since 0.3.0
 * 
 */
Authenticator.prototype.onSuccess = function( successCallback )
{
	
	this.successCallback = successCallback;
	
	return this;
	
};

/**
 * 
 * @since 0.3.0
 * 
 */
Authenticator.prototype.onError = function( errorCallback )
{
	
	this.errorCallback = errorCallback;
	
	return this;
	
};

/**
 * 
 * @since 0.3.0
 * 
 */
Authenticator.prototype.onCompletion = function( completionCallback )
{
	
	this.completionCallback = completionCallback;
	
	return this;
	
};

/*
 * 
 * Getters & Setters
 * 
 * I don't feel like documenting this right now... I'll do it later
 * 
 */

/**
 * 
 * @since 0.3.0
 * 
 */
Authenticator.prototype.getSuccessCallback = function( )
{
	
	return this.successCallback;
	
};

/**
 * 
 * @since 0.3.0
 * 
 */
Authenticator.prototype.getErrorCallback = function( )
{
	
	return this.errorCallback;
	
};

/**
 * 
 * @since 0.3.0
 * 
 */
Authenticator.prototype.getCompletionCallback = function( )
{
	
	return this.completionCallback;
	
};

/**
 * 
 * @since 0.3.0
 * 
 */
Authenticator.prototype.getType = function( )
{
	
	return this.type;
	
};

/**
 * 
 * @since 0.3.0
 * 
 */
Authenticator.prototype.setType = function( type )
{
	
	this.type = type;
	
};

/**
 * 
 * @since 0.3.0
 * 
 */
Authenticator.prototype.getPassphrase = function( )
{
	
	return this.passphrase;
	
};

/**
 * 
 * @since 0.3.0
 * 
 */
Authenticator.prototype.setPassphrase = function( passphrase )
{
	
	this.passphrase = passphrase;
	
};

/**
 * 
 * @since 0.3.0
 * 
 */
Authenticator.prototype.getIP = function( )
{
	
	return this.ip;
	
};

/**
 * 
 * @since 0.3.0
 * 
 */
Authenticator.prototype.setIP = function( ip )
{
	
	this.ip = ip;
	
};

module.exports = Authenticator;