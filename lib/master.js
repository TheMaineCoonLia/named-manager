/**
 * 
 * @since 0.3.0
 * 
 */
function Master( manager )
{

	var app = require( 'http' ).createServer( function(req, res)
	{
	
		res.writeHead( 403 );
		res.end( '403 - Unauthorized' );
		
	} );
	app.listen( manager.getConfig( ).get( 'port' ) );
	var io = require( 'socket.io' )( app );
	this.setIO( io );
	this.sockets = [ ];
	this.setupListeners( );
	console.log( '[Master] [Socket.IO] Running on port ' + manager.getConfig( ).get( 'port' ) );
	
}

/**
 * 
 * @since 0.3.0
 * 
 */
Master.prototype.setupListeners = function( )
{
	
	var self = this;
	
	this.getIO( ).sockets.on( 'connection', function( socket )
	{
		
		try
		{
		
			self.addFunctions( socket );
			
		}
		catch( exception )
		{
			
			console.log(
					
				'[Master] [Socket.IO] An exception occoured while adding the functions to the socket! {e: %o }',
				exception
					
			);
			
			socket.emit( 'close', 'Internal Error' );
			socket.disconnect( );
			
			return;
			
		}
		
		self.getSockets( ).push( socket );
		console.log( '[Master] [Socket.IO] [%s] Connected!', socket.getIP( ) );
		
		socket.on( 'authentication', function( data )
		{
			
			self.authenticate( data.type, data.passphrase, socket.getIP( ) )
				.onSuccess( function( type )
				{
					
					socket.setManagerType( type );
					console.log(
							
							'[Master] [Socket.IO] [%s] Authenticated as %s',
							socket.getIP( ),
							type
							
					);
					
				} )
				.onError( function( error )
				{

					console.log(
							
							'[Master] [Socket.IO] [%s] Authentication error! %s',
							socket.getIP( ),
							error.message
							
					);
					
					self.disconnect( socket, 'Failed to authenticate!' );
					
				} ).execute( );
			
		} );
		
		try
		{
			
			socket.emit( 'message', 'authenticate' );
			
		}
		catch( exception )
		{
			
		}
		
	} );

};

/**
 * 
 * @since 0.3.0
 * 
 */
Master.prototype.addFunctions = function( socket )
{
	
	socket.getIP = function( )
	{
	
		var headers = this.handshake.headers;
		
		if( headers[ 'x-forwarded-for' ] !== undefined )
		{
			
			return headers[ 'x-forwarded-for' ];
			
		}
		
		return this.request.connection._peername.address;
		
	};
	
	socket.isAuthenticated = function( )
	{
		
		return this.getManagerType( ) !== undefined;
		
	};
	
	socket.setManagerType = function( type )
	{
		
		this.managertype = type;
		
	};
	
	socket.getManagerType = function( type )
	{
		
		return this.managertype;
		
	};
	
};

/**
 * 
 * @since 0.3.0
 * 
 */
Master.prototype.authenticate = function( type, passphrase, ip )
{
	
	var Authenticator = require( './auth/authenticator.js' );
	
	return new Authenticator( type, passphrase, ip );
	
};

/**
 * 
 * @since 0.3.0
 * 
 */
Master.prototype.disconnect = function( socket, reason )
{
	
	console.log(
			
			'[Master] [Socket.IO] [%s] Disconnecting: %s',
			socket.getIP( ),
			reason
			
	);
	socket.emit( 'close', reason );
	socket.disconnect( );
	
	this.getSockets( ).filter( function( element )
	{
		
		if( element.id === undefined )
		{
		
			return false;
			
		}
		
		return element.id !== socket.id;
		
	} );
	
};

/*
 * 
 * Getters & Setters
 * 
 */

/**
 * 
 * @since 0.3.0
 * 
 */
Master.prototype.getIO = function( )
{

	return this.io;

};

/**
 * 
 * @since 0.3.0
 * 
 */
Master.prototype.setIO = function( io )
{
	
	this.io = io;
	
};

/**
 * 
 * @since 0.3.0
 * 
 */
Master.prototype.getSockets = function( )
{

	return this.sockets;
	
};

module.exports = Master;