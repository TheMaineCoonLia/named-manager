//Load the package.json into memory
process.pkg = require( '../package.json' );

//Tell the console we're starting and what version we're starting
console.log( 'Starting Named Manager v' + process.pkg.version );

//Require the modules needed for startup
var nconf = require( 'nconf' ),
	path = require( 'path' ),
	fs = require( 'fs' );

//Setup nconf to use command line arguments and then environment variables for the configuration
nconf.argv( ).env( );

//Setup the default settings
nconf.defaults(
{

	configFile: '/config.json',
	port: 6362,
	kind: 'master'
	
} );

//set the config path to a variable
var configFile = path.join( path.join( __dirname, '/..' ), nconf.get( 'configFile' ) );

//Check if it exists
if( !fs.existsSync( configFile ) )
{

	//Tell the console we can't proceed
	console.error( 'Config file ' + configFile + ' doesn\'t exist!' );
	
	//Rage quit
	process.exit( 1 );
	
	return;

}

//Use the config file
nconf.file( configFile );

/**
 * 
 * Construct a new Named Manager object
 * 
 * @param the nconf object to use
 * @since 0.1.0
 * 
 */
function NamedManager( config )
{
	
	//Check if the configuration is undefined
	if( config === undefined )
	{
	
		//Throw an error
		throw new Error( 'The config object cannot be null!' );
		
	}

	//Make it global
	global.NamedManager = this;
	//Store the config
	this.config = config;
	
	
	//Figure out what to do
	switch( this.getConfig( ).get( 'kind' ) )
	{
	
		default:
			
			//Setup the master
			var Master = require( './master.js' );
			this.setMaster( new Master( this ) );
			
			break;
	
	}

}

/*
 * 
 * Getters & Setters
 * 
 */

/**
 * 
 * This function is used to get the configuration object stores
 * 
 * @returns the configuration object
 * @since 0.1.0
 * 
 */
NamedManager.prototype.getConfig = function( )
{
	
	return this.config;
	
};

/**
 * 
 * @since 0.2.0
 * 
 */
NamedManager.prototype.getMaster = function( )
{
	
	return this.master;
	
};

/**
 * 
 * @since 0.2.0
 * 
 */
NamedManager.prototype.setMaster = function( master )
{
	
	this.master = master;
	
};

//Export the named manager
module.exports = NamedManager;
var manager = new NamedManager( nconf );